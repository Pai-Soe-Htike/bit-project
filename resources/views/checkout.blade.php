@extends('layouts/mainlayout')

@section('content')

	<div class="container shadow p-5 mb-5">
		@if(session('closed'))
			<div class="alert alert-danger">
			    {{ session('closed') }}
			</div>
		@endif
		<h3 class="colstyle1logo text-center">Checkout</h3>
		<hr>
		{{ Form::model($user, [ 
				'url'=> 'store', 
				'method' => 'POST' 

				]) }}
				<div class="form-group">
					{{ Form::label(null,'Name') }}
					{{ Form::text('name', null, [
						'class' => ($errors->has('name')? 'form-control is-invalid': 'form-control'),
						 'placeholder' => "Enter Name",
						 'required' => 'required',
						 'pattern' => '^[a-zA-Z1-9].*' 
						]) }}

					@if($errors->has('name'))
						<span class="invalid-feedback" role="alert">
							<strong>
								{{ $errors->first('name') }}
							</strong>
						</span>
					@endif
				</div>
				<div class="form-group">
					{{ Form::label(null,'Email') }}
					{{ Form::email('email', null, [
						'class' => ($errors->has('email')? 'form-control is-invalid': 'form-control'),
						 'placeholder' => "Enter Email",
						 'required' => 'required'
						]) }}

					@if($errors->has('email'))
						<span class="invalid-feedback" role="alert">
							<strong>
								{{ $errors->first('email') }}
							</strong>
						</span>
					@endif
				</div>
				<div class="form-group">
					{{ Form::label(null,'Phone Number') }}
					{{ Form::text('phone', null, [
						'class' => ($errors->has('phone')? 'form-control is-invalid': 'form-control'),
						 'placeholder' => "Enter Phone No",
						 'required' => 'required'
						]) }}
					@if($errors->has('phone'))
						<span class="invalid-feedback" role="alert">
							<strong>
								{{ $errors->first('phone') }}
							</strong>
						</span>
					@endif
				</div>

				<div class="form-group">
					{{ Form::label(null,'Address') }}
					{{ Form::text('address', null, [
						'class' => ($errors->has('address')? 'form-control is-invalid': 'form-control'),
						 'placeholder' => "Enter Your Address",
						 'required' => 'required',
						 'pattern' => '^[a-zA-Z1-9].*'
						]) }}
					@if($errors->has('address'))
						<span class="invalid-feedback" role="alert">
							<strong>
								{{ $errors->first('address') }}
							</strong>
						</span>
					@endif
				</div>

				<div class="form-group">
					@if($type == 'order')
					<input type="radio" name="type" value="Order" checked> Order
					<input type="radio" name="type" value="PickUp"> Pick Up
					@else
					<input type="radio" name="type" value="Order" > Order &nbsp;
					<input type="radio" name="type" value="PickUp" checked> Pick Up
					@endif

				</div>
				<?php
					date_default_timezone_set("Asia/Yangon");
					$timenow = date("H:i");
					$opentime = "17:00";
					$closetime = "23:00";
					$deliverytime = date('H:i',strtotime('+1 hour',strtotime($timenow)));
					$deliverytime1 = date('H:i',strtotime('+2 hour',strtotime($timenow)));
					$deliverytime2 = date('H:i',strtotime('+3 hour',strtotime($timenow)));
					$deliverytime3 = date('H:i',strtotime('+4 hour',strtotime($timenow)));
					$deliverytime4 = date('H:i',strtotime('+5 hour',strtotime($timenow)));

				?>
				<div class="form-group">
					<label>Date</label>
					<input type="text" class="form-control" name="date" value="{{ date('y-m-d') }}" readonly>
				</div>
				<div class="form-group">
					<label>Choose Time</label>
					<select class="form-control" name="time">
						@if($timenow > $closetime || $timenow <= $opentime)
						<option value="close">Closed</option>
						@else
						<option value="{{$timenow}}">{{$timenow}}</option>

						<option value="{{ $deliverytime1 }}">{{ $deliverytime1 }}</option>

						<option value="{{ $deliverytime2 }}">{{ $deliverytime2 }}</option>

						<option value="{{ $deliverytime3 }}">{{ $deliverytime3 }}</option>

						<option value="{{ $deliverytime4 }}">{{ $deliverytime4 }}</option>
						@endif
					</select>
				</div>

				{{ Form::hidden('orderdate', date('y-m-d')) }}
				@if($timenow > $closetime || $timenow <= $opentime)
					<button class="btn btn-primary" disabled="disabled">CheckOut</button>
					<p style="color: red;">Cannot Available rightnow!</p>
				@else
					<button class="btn btn-primary">CheckOut</button>
				@endif

		{{ Form:: close() }}
		@if (session('email'))
		    <div class="alert alert-success">
		        alert('{{ session('email') }}');
		    </div>
		@endif
	</div>
@endsection
