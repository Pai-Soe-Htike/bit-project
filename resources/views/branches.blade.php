@extends('layouts/mainlayout')

@section('content')
	<div class="container mt-5" style="font-family: 'Open Sans', sans-serif;">
		<div class="row">
			@forelse( $restaurants as $restaurant )
			<div class="col-md-3">
			  	<div class="card mb-3" style="box-shadow: 0 10px 10px -5px;">
					<img src="{{asset('uploads/'.$restaurant->image_name)}}" class="card-img-top" alt="{{$restaurant->image_name}}" style="transition: opacity 3s ease-in-out; height: 150px;">
				    <div class="card-body text-center">
				      	<h5 class="card-title" style="font-size: 17px;">{{ $restaurant->name }}</h5>
				      	<div class="d-flex justify-content-between">
				      		<p class="card-text flex-items mb-0 mt-2">Minimum Price - Ks {{ $restaurant->min_price }}</p>
				      	</div>
				    </div>
				    <div class="card-footer text-center">
				        <a href="{{ route('branch.detail', ['id' => $restaurant->id, 'type' => 'order']) }}" class="btn btn-primary" style="font-size: 10px;">Order</a>
				        <a href="{{ route('branch.detail', ['id' => $restaurant->id, 'type' => 'pickup']) }}" class="btn btn-primary" style="font-size: 10px;">Pickup</a>
				    </div>
			  	</div>
			</div>
			@empty
			<h1>No availave restaurant!</h1>
			@endforelse
		</div>
	</div>
@endsection