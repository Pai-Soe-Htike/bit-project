<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

	<title>PDF</title>
</head>
<body>
	<strong>Name : {{ $pdfuname }}</strong>
	<br>
	<strong>Email : {{ $email }}</strong>
	<br>
	<strong>Phone : {{ $phone }}</strong>	 
	 <br>
	 <strong>Delivery Charge : Ks 1000</strong>	 
	 <br>
	
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">Id</th>
	      <th scope="col">Name</th>
	      <th scope="col">Quantity</th>
	      <th scope="col">Order Tpye</th>
	      <th scope="col">Price</th>	      
	      	      
	    </tr>
	  </thead>
	  <tbody>
	    <?php
	    	$gtotal = 0; 
	    ?>

    	@foreach(session('cart') as $cart => $details)
    	  <tr>
	    	<?php 
		    	$totalq = 0;
		    	
                $totalp = $details['quantity'] * $details['price'];
        
            ?>
            	<td>{{  $checkout->name }}</td>
		      	<td>{{ $details['name'] }}</td>
		      	<td>{{  $totalq += $details['quantity'] }}</td>	
		      	<td>{{  $type }}</td>	      	
		      	<td>{{  $totalp }}</td>
		      	
	      </tr>
      	@endforeach
      	<th colspan="4">Total Price</th>
      	<td>
      		@foreach(session('cart') as $detail)
      			<?php $gtotal += $detail['quantity'] * $detail['price'] ?>			  	
      		@endforeach
      		{{ $gtotal + 1000 }}
      	</td>
		    
	  </tbody>
	</table>
</body>
</html>