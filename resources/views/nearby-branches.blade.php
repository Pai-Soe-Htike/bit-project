@extends('layouts/mainlayout')

@section('content')
	<div class="container mt-5" style="font-family: 'Open Sans', sans-serif;">
		<div class="row">
			@forelse( $nearbyBranches as $nearbyBranch )
			<div class="col-md-3">
			  	<div class="card mb-3" style="box-shadow: 0 10px 10px -5px;">
					@if(empty($nearbyBranch['image_name']))
						<img src="{{asset('images/default.jpg')}}" class="card-img-top" alt="default image" style="transition: opacity 3s ease-in-out; height: 150px;">
					@else
						<img src="{{asset('uploads/'.$nearbyBranch['image_name'])}}" class="card-img-top" alt="{{$nearbyBranch['image_name']}}" style="transition: opacity 3s ease-in-out; height: 150px;">
					@endif
				    <div class="card-body text-center">
				      	<h5 class="card-title" style="font-size: 17px;">{{ $nearbyBranch['name'] }}</h5>
				      	<div class="justify-content-between text-left">
				      		<p class="card-text mb-0 mt-2">Minimum Price - <strong>{{ $nearbyBranch['min-price'] }} MMK</strong></p>
				      		<p class="card-text mb-0 mt-2">Distance - <strong>{{ $nearbyBranch['dis'] }} mile/s</strong></p>
				      	</div>
				    </div>
				    <div class="card-footer text-center">
				        <a href="{{ route('nearby.detail', ['id' => $nearbyBranch['id'], 'type' => 'order']) }}" class="btn btn-primary" style="font-size: 10px;">Order</a>
				        <a href="{{ route('nearby.detail', ['id' => $nearbyBranch['id'], 'type' => 'pickup']) }}" class="btn btn-primary" style="font-size: 10px;">Pickup</a>
				    </div>
			  	</div>
			</div>
			@empty
			<h1>No availave restaurant!</h1>
			@endforelse
		</div>
	</div>
@endsection