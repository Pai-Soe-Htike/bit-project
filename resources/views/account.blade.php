@extends('layouts/adminlayout')
	@section('content')
	@if(session()->has('danger'))
		<div class="alert alert-danger">
			{{ session('danger') }}
		</div>
	@endif
	@if(session()->has('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
	@endif
	<div class="card position-relative mt-2">
	  	<div class="card-header">
	  		<h3>Manage Account</h3>
	  	</div>
	  	<div class="card-body">
		   <form action="{{ route('user.profile.edit', Auth::user()->id) }}" method="POST">
		   		{{ csrf_field() }}
		   		<input type="hidden" name="id" value="{{ Auth::user()->id }}">
		   		<div class="form-group">
		   			<label>Name</label>
		   			<input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" >
		   		</div>
		   		<div class="form-group">
		   			<label>Email</label>
		   			<input type="email" class="form-control" value="{{ Auth::user()->email }}" readonly>
		   		</div>
		   		<div class="form-group">
		   			<label>Old Password</label>
		   			<input type="password" name="old_pw" class="form-control">
		   		</div>
		   		<div class="form-group">
		   			<label>New Password</label>
		   			<input type="password" name="new_pw" class="form-control">
		   		</div>
		   		<button class="btn btn-success">Update</button>
		   </form>
	  	</div>
	</div>
	@endsection