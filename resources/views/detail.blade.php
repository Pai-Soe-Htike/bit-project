@extends('layouts/mainlayout')

@section('content')

	{{-- {{ dd( $restaurant_id )}} --}}
	<div class="container mt-5" style="font-family: 'Open Sans', sans-serif;">
		<div class="row">
			<div class="col-md-9 border-right">
				<div class="row">
					@forelse( $menus as $menu )
					<div class="col-md-6">
					  	<div class="card mb-3" style="box-shadow: 0 10px 10px -5px;">
	  						@if(empty($menu->image_name))
	  							<img src="{{asset('images/default.jpg')}}" class="card-img-top" alt="default image" style="transition: opacity 3s ease-in-out; height: 150px;">
	  						@else
	  							<img src="{{asset('uploads/'.$menu->image_name)}}" class="card-img-top" alt="{{$menu->image_name}}" style="transition: opacity 3s ease-in-out; height: 150px;">
	  						@endif
	  					    <div class="card-body text-center">
	  					      	<h5 class="card-title" style="font-size: 17px;">{{ $menu->name }}</h5>
	  					      	<div class="justify-content-between text-left">
	  					      		<p class="card-text mb-0 mt-2">Price - <strong>{{ $menu->price }} MMK</strong></p>
	  					      		<p class="card-text mb-0 mt-2">Category - <strong>{{ $menu->category->name }}</strong></p>
	  					      	</div>
	  					    </div>
	  					    <div class="card-footer text-center">
	  				        	<button class="showhidden btn btn-success" id="{{ $menu->id }}" ><a href="	{{ route('cartadd', $menu->id) }}">Add Item</a></button>
	  					    </div>
	  				  	</div>
					</div>
					@empty
					<h1>No availave menus!</h1>
					@endforelse
				</div>
			</div>
			<div class="col-md-3">
				<h6 class="border p-1">Delivery Charge: Ks 1000</h6>
				<h6 class="border p-1">Est delivery time: 1 hour</h6>
				<div class="form-group">
						<div class="dropdown">
							  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							   Filter with category
							  </button>
							  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							  	@foreach( $categories as $menu )
							    	<a class="dropdown-item" href="{{ url('category', [$restaurant_id, $menu->id, $ordertype]) }}">{{ $menu->name }}</a>
							    @endforeach
							  </div>
						</div>
				</div>
				@if(session('success'))
					<div class="alert alert-success text-center">	
						{{ session('success') }}
					</div>
				@endif
				@if(session('cart'))				
				<table class="table table-bordered mt-3">
				  <thead>
				    <tr>
				      <th scope="col">Name</th>
				      <th scope="col">Qty</th>
				      <th scope="col">Price</th>
				    </tr>
				  </thead>
				  <tbody>
			    	@foreach(session('cart') as $id => $details)
			    		<tr>
			    	 		<td>{{ $details['name']}}</td>
			    			<td>{{ $details['quantity']}}</td>
			    			<?php 	
			    					$total = $details['quantity'] * $details['price']
			    			 ?>

			    			<td>{{ $total }}</td>
			    		</tr>
			    	@endforeach				    	
				  </tbody>
				  <th colspan="2">Grand Total</th>
				  <?php 
				  	$gtotal = 0;
				   ?>
				  @foreach(session('cart') as $detail)
				  	<?php $gtotal += $detail['quantity'] * $detail['price'] ?>			  	
				  @endforeach
				  <th>{{ $gtotal + 1000 }}</th>
				</table>
				<div class="row btnfz">
					<div class="col-md-4">
						<a href="{{ route('cart.alldelete')}}" class="btn btn-danger">Empty Cart</a>
					</div>
					<div class="col-md-4">
						<a href="{{ route('viewcart',['id' => $restaurant_id, 'type' => $ordertype]) }}" class="btn btn-primary">View Cart</a>
					</div>
					<div class="col-md-4">
						<a href="{{ route('checkout', $ordertype) }}"class="btn btn-success">Check Out</a>
					</div>
				</div>
				@endif				
			</div>
		</div>

	@section('script')
		  <script>
		  	$('.card-hide').hide();
		  	$('.showhidden').click(function () {
				$('.card-hide').show();
		  	});
		  </script>
	@endsection
	</div>

@endsection