@extends('layouts/mainlayout')

@section('content')
@if(session()->has('danger'))
	<div class="alert alert-danger">
		{{ session('danger') }}
	</div>
@endif
@if(session()->has('success'))
	<div class="alert alert-success">
		{{ session('success') }}
	</div>
@endif

<div class="container mt-4">
	<nav>
	  <div class="nav nav-tabs" id="nav-tab" role="tablist">
	    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Order History</a>
	    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Profile Setting</a>
	  </div>
	</nav>
	<div class="tab-content mt-3" id="nav-tabContent">
	  <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
	  		<table class="table">
	  		  <thead class="thead-dark">
	  		    <tr>
	  		      <th scope="col">Name</th>
	  		      <th scope="col">Price</th>
	  		      <th scope="col">Order Type</th>
	  		      <th scope="col">Date</th>
	  		      <th scope="col">Status</th>
	  		    </tr>
	  		  </thead>
	  		  <tbody>
	  		  	@foreach($user->orders as $order)
	  		  	<tr>
	  		  	  <td>{{ $order->name }}</td>
	  		  	  <td>{{ $order->totalprice }}</td>
	  		  	  <td>{{ $order->type }}</td>
	  		  	  <td>{{ $order->orderdate }}</td>
	  		  	  @if($order->status == 1)
	  		  	  	<td>Pending</td>
	  		  	  @else
	  		  	  	<td>Complete</td>
	  		  	  @endif
	  		  	</tr>
	  		  	@endforeach
	  		  </tbody>
	  		</table>
	  </div>
	  <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
  		<div class="card position-relative mt-5">
  		  	<div class="card-header">
  		  		<h3>Manage Account</h3>
  		  	</div>
  		  	<div class="card-body">
  			   <form action="{{ route('user.profile.edit', Auth::user()->id) }}" method="POST">
  			   		@csrf
  			   		<input type="hidden" name="id" value="{{ Auth::user()->id }}">
  			   		<div class="form-group">
  			   			<label>Name</label>
  			   			<input type="text" name="name" value="{{ Auth::user()->name }}" class="form-control" >
  			   		</div>
  			   		<div class="form-group">
  			   			<label>Email</label>
  			   			<input type="email" class="form-control" value="{{ Auth::user()->email }}" readonly>
  			   		</div>
  			   		<div class="form-group">
  			   			<label>Old Password</label>
  			   			<input type="password" name="old_pw" class="form-control">
  			   		</div>
  			   		<div class="form-group">
  			   			<label>New Password</label>
  			   			<input type="password" name="new_pw" class="form-control">
  			   		</div>
  			   		<button class="btn btn-success">Update</button>
  			   </form>
  		  	</div>
  		</div>
	  </div>
	</div>
</div>
@endsection