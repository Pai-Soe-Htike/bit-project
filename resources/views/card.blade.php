@extends('layouts/mainlayout')

@section('content')
	<?php $gtotal = 0; ?>
	@if(session('cart'))
		<div class="container pt-3 mb-9">
			<table class="table table-bordered text-center">
			   <thead>
			    <tr>
			      <th scope="col">Image</th>
			      <th scope="col">Name</th>
			      <th scope="col">Qty</th>
			      <th scope="col">Price</th>
			      <th scope="col">Action</th>
			    </tr>
			  </thead>
			  <tbody>
			    @foreach(session('cart') as $id => $details)
		    		<tr>
		    	 		<td>
		    	 			@if(empty($details['image']))
	  							<img src="{{asset('images/default.jpg')}}" class="card-img-top" alt="default image" style="transition: opacity 3s ease-in-out; width: 50px;">
	  						@else
	  							<img src="{{asset('uploads/'.$details['image'])}}" class="card-img-top" style="transition: opacity 3s ease-in-out; width: 50px;">
	  						@endif
		    	 		</td>
		    	 		<td>{{ $details['name']}}</td>
		    			<td data-th="">
		    				<button type="button" id="sub" class="sub update-cart" data-id="{{ $id }}">-</button>
						    <input type="number" class="quantity text-center" name="quantity" id="1" value="{{ $details['quantity']}}" min="0"/>
						    <button type="button" id="add" class="add update-cart" data-id="{{ $id }}">+</button>
		    			</td>
		    			<?php 	
		    					$total = $details['quantity'] * $details['price'];
		    					$gtotal += $details['quantity'] * $details['price'];
		    			 ?>

		    			<td class="text-right">Ks {{ $total }}</td>
		    			<td>
		    				<button class="btn btn-danger btn-sm remove-from-cart" data-id="{{ $id }}"><i class="fas fa-trash-alt"></i></button>
		    			</td>
		    		</tr>
		    	@endforeach	
		    		<tr>
		    			<td colspan="3">Delivery Charge</td>
		    			<td colspan="2">Ks 1000</td>
		    		</tr>
		    		
		    		<tr>
		    			<th colspan="3">Grand Total</th>
		    			<th colspan="2"><strong>Ks {{ $gtotal + 1000 }}</strong></th>
		    		</tr>
		    		
			  </tbody>
			</table>
			<div class="float-right">
				@if($order_type == 'order')
				<button class="btn btn-primary"><a href="{{ route('branch.detail', ['id' => $restaurant_id, 'type' => 'order']) }}" class="text-light">Add Foods</a></button>
				@else
				<button class="btn btn-primary"><a href="{{ route('branch.detail', ['id' => $restaurant_id, 'type' => 'pickup']) }}" class="text-light">Add Foods</a></button>
				@endif
				<button class="btn btn-danger"><a href="{{ route('cart.alldelete')}}" class="text-light">Empty Cart</a></button>
				<a href="{{ route('checkout', $order_type) }}"><button class="btn btn-success">Check Out</button></a>
			</div>
		</div>
	@else
	<div class="container text-center shadow p-5 mb-5">
		<h1 class="text-danger">Your Cart is Empty</h1>
		<button class="btn btn-success"><a href="{{ url('/') }}" class="text-light">Continute Shopping</a></button>
	</div>
	@endif
	<script type="text/javascript">

		$('.add').click(function () {
				if ($(this).prev().val()) {
		    	$(this).prev().val(+$(this).prev().val() + 1);
				}
		});
		$('.sub').click(function () {

				if ($(this).next().val() > 1) {
		    	if ($(this).next().val() > 1) $(this).next().val(+$(this).next().val() - 1);
				} 
		});

		$(".update-cart").click(function (e) {
              e.preventDefault();

              var ele = $(this);

               $.ajax({
                  url: '{{ url('update-cart') }}',
                  method: "patch",
                  data: {

                        _token: '{{ csrf_token() }}', 
                        id: ele.attr("data-id"), 
                        quantity: ele.parents("tr").find(".quantity").val()},

                  success: function (response) {
                      window.location.reload();
                  }
               });
           });
		$(".remove-from-cart").click(function (e) {
               e.preventDefault();

               var ele = $(this);

               if(confirm("Are you sure")) {
                   $.ajax({
                       url: '{{ route('cart.remove-from-cart') }}',
                       method: "DELETE",
                       data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
                       success: function (response) {
                           window.location.reload();
                       }
                   });
               }
           });
	</script>
@endsection