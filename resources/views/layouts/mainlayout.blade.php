<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>GDNB</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="{{ asset('/images/logo1.png') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Argon CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">    
</head>

<body>
    <div class="d-flex bd-highlight mb-3 bg-light shadow">
        <div class="mr-auto p-2 bd-highlight">
            <a href="{{ url('/') }}">
                <img src="{{ url('/images/logo1.png') }}" width="100px" height="auto" alt="">
            </a>
        </div>
        @guest
            <div class="p-2 bd-highlight">
                <a href="">
                  <i class="fas fa-shopping-bag" style=""></i>
                </a>
            </div>
            <div class="p-2 bd-highlight">
                <a class="nav-link text-uppercase" href="{{ route('login') }}">login</a>
            </div>
            <div class="p-2 bd-highlight">
                <a class="nav-link text-uppercase" href="{{ route('register') }}">register</a>
            </div>
        @else
            <div class="p-2 bd-highlight dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration: none; color: #9e9797; font-weight: bold;">
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <div class="dropdown-menu mr-5" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">Action</a>
                  @if( Auth::user()->role_id == 1 )
                    <a class="dropdown-item" href="{{ url('admin/restaurants') }}">Dashboard</a>
                  @else
                    <a class="dropdown-item" href="{{ url('user/profile') }}">Profile</a>
                  @endif
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                      Logout
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>
                </div>
            </div>
        @endguest
    </div>
    @yield('content')
    <div class="footer container-fluid" style="background: #515651;">
      <div class="row">
        <div class="col-md-9">
          <p style="color: #fff; font-weight: bold;">GDB On Social</p>
          <ul class="list-group list-group-horizontal">
            <li class="list-group-item" id="fb">Facebook</li>
            <li class="list-group-item" id="ig">Instagram</li>
            <li class="list-group-item" id="tt">Twitter</li>
            <li class="list-group-item" id="li">Linkedin</li>
          </ul>
        </div>
        <div class="col-md-3">
          <input type="text" placeholder="Email" style="border-radius: 10px; border: 1px solid #f47c48; padding: 3px 5px;">
          <button style="background: transparent; color: #fff; border-radius: 16px; cursor: pointer;">Subscribe</button>
          <p class="mt-3" style="color: #3e3e3e;">paisoehtike&copy;2019</p>
        </div>
      </div>
    </div>

    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.0.min.js"
  integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
  crossorigin="anonymous"></script>
    @yield('script')
</body>

</html>