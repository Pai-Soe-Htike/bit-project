@extends('layouts.adminlayout')


@section('title')
	Order Details
@endsection

@section('content')
	<h3>{{ $user->name }}'s Order Detail</h3>
	<hr>

	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">Product Name</th>
	      <th scope="col">Quantity</th>
	      <th scope="col">Price</th>
	    </tr>
	  </thead>
	  <tbody>
	  		@foreach($user->checkout_details as $order)
		    <tr>
		      	<td>{{ $order->name }}</td>
		      	<td>{{  $order->quantity }}</td>
		      	<td>{{  $order->price }}</td>
		    </tr>
		    @endforeach
	  </tbody>
	</table>

@endsection