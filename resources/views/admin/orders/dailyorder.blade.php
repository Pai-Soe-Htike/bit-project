@extends('layouts.adminlayout')

@section('title')
	Dailyorder
@endsection

@section('content')

	<h3>Today Order</h3>
	<hr>
	<ul class="nav">
	  <li class="nav-item">
	    <a class="nav-link active btn btn-primary" href="{{ url('pdf') }}">Generatepdf</a>
	  </li>
	</ul>
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">User's Name</th>
	      <th scope="col">Quantity</th>	      
	      <th scope="col">Ordered Date</th>
	      <th scope="col">Price</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	<?php  $total = 0; ?>
	  	@foreach($todayorder as $order)
	  		<?php $total += $order->totalprice ?>
		    <tr>
		      	<td>{{ $order->user->name }}</td>
		      	<td>{{  $order->totalquantity }}</td>
		      	<td>{{  $order->orderdate }}</td>		      	
		      	<td>{{  $order->totalprice }}</td>
		      	<td>
		      		<a href="{{ url('orderdetail', $order->user->id) }}" class="btn btn-primary">Detail</a>
		      	</td>

		    </tr>		    
	    @endforeach	    
	  </tbody>
	  <tr>
	    	<td></td>
	    	<td></td>
	    	<th scope="col">Total</th>
	    	<th scope="col">{{ $total }}</th>
	  </tr>
	</table>
@endsection