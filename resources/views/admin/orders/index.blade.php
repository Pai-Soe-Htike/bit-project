@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	@if(session()->has('message'))
			<div class="alert alert-{{ session('status') }} mt-2" role="alert">
				{{ session('message') }}
			</div>
		@endif
		<div style="display: flex">
			<h3 class="mr-3">Orders</h3>
			<a href="{{ url('dailyorder') }}" class="btn btn-primary">Daily Order</a>
		</div>
	<hr>
	<table class="table">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">Order ID</th>
	      <th scope="col">Email</th>
	      <th scope="col">Name</th>
	      <th scope="col">Total Quantity</th>
	      <th scope="col">Total Price</th>
	      <th scope="col">Order Type</th>
	      <th scope="col">Order Date</th>
	      <th scope="col">Order Time</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($orders as $order)
	    <tr>
	      <td>{{ $order->name }}</td>
	      <td>{{ $order->user->email }}</td>
	      <td>{{ $order->user->name }}</td>		    
	      <td>{{ $order->totalquantity }}</td>		    
	      <td>{{ $order->totalprice }}</td>       		     
	      <td>{{ $order->type }}</td>
	      <td>{{ $order->orderdate }}</td>		     
	      <td>{{ $order->ordertime }}</td>		     
	      <td>
	    	<a href="{{ url('orderdetail', $order->user->id) }}" class="btn btn-primary">Detail</a>
	      </td>   
	    </tr>
	    @endforeach
	  </tbody>
	</table>

	
@endsection