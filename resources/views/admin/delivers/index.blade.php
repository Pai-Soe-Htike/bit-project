@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	@if(session()->has('message'))
			<div class="alert alert-{{ session('status') }} mt-2" role="alert">
				{{ session('message') }}
			</div>
		@endif
		<div style="display: flex">
			<h3 class="mr-3">Delivers</h3>
		</div>
	<hr>
	<table class="table">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">Order ID</th>
	      <th scope="col">Email</th>
	      <th scope="col">Name</th>
	      <th scope="col">Total Quantity</th>
	      <th scope="col">Total Price</th>
	      <th scope="col">Order Type</th>
	      <th scope="col">Status</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($orders as $order)
	    <tr>
	      <td>{{ $order->name }}</td>
	      <td>{{ $order->user->email }}</td>
	      <td>{{ $order->user->name }}</td>		    
	      <td>{{ $order->totalquantity }}</td>		    
	      <td>{{ $order->totalprice }}</td>		     
	      <td>{{ $order->type }}</td>		     
	      <td>
	      	@if($order->status == 1)
	      		Pending
	      	@else
	      		Complete
	      	@endif
	      </td>		     
	      <td>
	      	<a href="{{ route('update.delivery.status', $order->id) }}" class="btn btn-success">Update Status</a>
	      </td>   
	    </tr>
	    @endforeach
	  </tbody>
	</table>

	
@endsection