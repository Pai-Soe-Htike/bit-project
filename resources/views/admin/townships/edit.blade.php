@extends('layouts.adminlayout')
<!-- @section('title')
	Township Edit
@endsection -->
@section('content')
	<h3>Township Edit</h3>
	<hr>
	{{ Form::model($township, [ 
		'route' => ['townships.update',$township->id],
		 'method' => 'PUT' ]) }}
		<div class="form-group">
			{{ Form::label(null, 'Name') }}
			{{ Form::text('name', null,[
				'class' => ($errors->has('name')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('name') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Description') }}
			{{ Form::text('description', null,[
				'class' => ($errors->has('description')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('description'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('description') }}
					</strong>
				</span>
			@endif
		</div>	
		<button class="btn btn-success">Update</button>
	{{ Form::close() }}
@endsection