@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	@if(session()->has('message'))
		<div class="alert alert-{{ session('status') }} mt-2" role="alert">
			{{ session('message') }}
		</div>
	@endif
	<div style="display: flex">
		<h3 class="mr-3">Townships</h3>
		<a href="{{ route('townships.create') }}" class="btn btn-success">Add</a>
	</div>
	<hr>
	<table class="table">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">Name</th>
	      <th scope="col">Description</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($townships as $township)
	    <tr>
	      <td>{{ $township->name }}</td>
	      <td>{{ $township->description }}</td>
	      <td>	      	
	      	{{ Form::model($township, [
	      		'route' => [ 'townships.destroy', $township->id ],
	      		'method' => 'DELETE',
	      		'class' => 'd-inline'
	      		]) }}
	      		<a href="{{ route('townships.show', $township->id ) }}" class="btn btn-success">Detail</a>
	      		<a href="{{ route('townships.edit', $township->id) }}" class="btn btn-primary">Edit</a>
	      		<button class="btn btn-danger">Delete</button>
	      	{{ Form::close() }}
	      </td>   
	    </tr>
	    @endforeach
	  </tbody>
	</table>

	
@endsection