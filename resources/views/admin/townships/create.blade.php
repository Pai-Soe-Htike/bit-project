@extends('layouts.adminlayout')
<!-- @section('title')
	Township
@endsection -->
@section('content')
	<h3>Township Create</h3>
	<hr>
	{{ Form::open([ 'route' => 'townships.store', 'method' => 'POST' ]) }}
		<div class="form-group">
			{{ Form::label(null, 'Name') }}
			{{ Form::text('name', null,[
				'class' => ($errors->has('name')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('name') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Description') }}
			{{ Form::text('description', null,[
				'class' => ($errors->has('description')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('description'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('description') }}
					</strong>
				</span>
			@endif
		</div>	
		<button class="btn btn-success">Create</button>
	{{ Form::close() }}
@endsection