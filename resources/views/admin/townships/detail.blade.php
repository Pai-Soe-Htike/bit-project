@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	<h3>{{ $township->name }} Detail</h3>
	{{ Form::model($township, [
		'route' => [ 'townships.destroy', $township->id ],
		'method' => 'DELETE',
		'class' => 'd-inline'
		]) }}
		<a href="{{ route('townships.edit', $township->id) }}" class="btn btn-primary">Edit</a>
		<button class="btn btn-danger">Delete</button>
	{{ Form::close() }}
	<hr>
	<div class="card text-center">
	  <div class="card-header">
	    {{ $township->name }}
	  </div>
	  <div class="card-body">
	    <h5 class="card-title">Name = {{ $township->name }}</h5>
	    <p class="card-text">Description = {{ $township->description }}</p>
	    <a href="javascript:history.back()" class="btn btn-primary">Go Home</a>
	  </div>
	  <div class="card-footer text-muted">
	    {{ Carbon\Carbon::parse($township->created_at)->diffForHumans() }}
	  </div>
	</div>
	
@endsection