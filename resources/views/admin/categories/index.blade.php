@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	@if(session()->has('message'))
		<div class="alert alert-{{ session('status') }} mt-2" role="alert">
			{{ session('message') }}
		</div>
	@endif
	<div style="display: flex">
		<h3 class="mr-3">Categories</h3>
		<a href="{{ route('categories.create') }}" class="btn btn-success">Add</a>
	</div>
	<hr>
	<table class="table">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">Name</th>
	      <th scope="col">Description</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($categories as $category)
	    <tr>
	      <td>{{ $category->name }}</td>
	      <td>{{ $category->description }}</td>
	      <td>	      	
	      	{{ Form::model($category, [
	      		'route' => [ 'categories.destroy', $category->id ],
	      		'method' => 'DELETE',
	      		'class' => 'd-inline'
	      		]) }}
	      		<a href="{{ route('categories.show', $category->id ) }}" class="btn btn-success">Detail</a>
	      		<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary">Edit</a>
	      		<button class="btn btn-danger">Delete</button>
	      	{{ Form::close() }}
	      </td>   
	    </tr>
	    @endforeach
	  </tbody>
	</table>	
@endsection