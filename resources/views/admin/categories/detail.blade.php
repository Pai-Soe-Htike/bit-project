@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	<h3>{{ $category->name }} Detail</h3>
	{{ Form::model($category, [
		'route' => [ 'categories.destroy', $category->id ],
		'method' => 'DELETE',
		'class' => 'd-inline'
		]) }}
		<a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary">Edit</a>
		<button class="btn btn-danger">Delete</button>
	{{ Form::close() }}
	<hr>
	<div class="card text-center">
	  <div class="card-header">
	    {{ $category->name }}
	  </div>
	  <div class="card-body">
	    <h5 class="card-title">Name = {{ $category->name }}</h5>
	    <p class="card-text">{{ $category->description }}</p>
	    <a href="javascript:history.back()" class="btn btn-primary">Go Home</a>
	  </div>
	  <div class="card-footer text-muted">
	  	{{ Carbon\Carbon::parse($category->created_at)->diffForHumans() }}
	  </div>
	</div>
	
@endsection