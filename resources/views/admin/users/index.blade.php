@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	@if(session()->has('message'))
			<div class="alert alert-{{ session('status') }} mt-2" role="alert">
				{{ session('message') }}
			</div>
		@endif
	<hr>
	<table class="table">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">Name</th>
	      <th scope="col">Email</th>
	      <th scope="col">Phone</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($users as $user)
	    <tr>
	      <td>{{ $user->name }}</td>
	      <td>{{ $user->email }}</td>
	      <td>{{ $user->phone }}</td>		     
	      <td><a href="{{ url('orderdetail', $user->id) }}" class="btn btn-primary">Detail</a></td>   
	    </tr>
	    @endforeach
	  </tbody>
	</table>

	
@endsection