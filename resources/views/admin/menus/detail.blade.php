@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	<h3>{{ $menu->name }} Detail</h3>
	{{ Form::model($menu, [
		'route' => [ 'menus.destroy', $menu->id ],
		'method' => 'DELETE',
		'class' => 'd-inline'
		]) }}
		<a href="{{ route('menus.edit', $menu->id) }}" class="btn btn-primary">Edit</a>
		<button class="btn btn-danger">Delete</button>
	{{ Form::close() }}
	<hr>
	<div class="card text-center">
	  <div class="card-header">
	    {{ $menu->name }}
	  </div>
	  <div class="card-body">
	    <h5 class="card-title">Name = {{ $menu->name }}</h5>
	    <p class="card-text">Price = {{ $menu->price }}</p>
	    <p class="card-text">Category =
	    	@if($menu->category)
	    		{{ $menu->category->name }}
	    	@else
	    		-
	    	@endif
	    </p>
	    <p class="card-text">Branches =
	    	@if($menu->restaurants) 
		    	@foreach($menu->restaurants as $restaurant)
		    		{{ $restaurant->name }},
		    	@endforeach
		    @else
		    	-
		    @endif
	    </p>
	     <a href="{{ $menu->image_url }}" target="_blank">
	        <img src="{{ asset('uploads/'.$menu->image_name) }}" class="img-responsive mb-2" alt="	{{$menu->image_name}}" style="width: 177px; height: 117px; border-radius: 5px; border: 1px solid #000;">
	    </a> <br>
	    <a href="javascript:history.back()" class="btn btn-primary">Back</a>
	  </div>
	  <div class="card-footer text-muted">
	    {{ Carbon\Carbon::parse($menu->created_at)->diffForHumans() }}
	  </div>
	</div>
	
@endsection