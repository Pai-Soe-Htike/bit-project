@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	@if(session()->has('message'))
			<div class="alert alert-{{ session('status') }} mt-2" role="alert">
				{{ session('message') }}
			</div>
		@endif
		<div style="display: flex">
			<h3 class="mr-3">Food Menus</h3>
			<a href="{{ route('menus.create') }}" class="btn btn-success">Add</a>
		</div>
	<hr>
	<table class="table">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">Name</th>
	      <th scope="col">Price</th>
	      <th scope="col">Category</th>
	      <th scope="col">Branches</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($menus as $menu)
	    <tr>
	      <td>{{ $menu->name }}</td>
	      <td>{{ $menu->price }}</td>
	      <td>
	      	@if($menu->category)
	      		{{ $menu->category->name }}
	      	@else
	      		-
	      	@endif
	      </td>		    
	      <td>
	      	@if($menu->restaurants)
		      	@foreach($menu->restaurants as $restaurant)
		      		{{ $restaurant->name }}, 
		      	@endforeach
		    @else
		    	-
		    @endif
	      </td>	 
	      <td>	      	
	      	{{ Form::model($menu, [
	      		'route' => [ 'menus.destroy', $menu->id ],
	      		'method' => 'DELETE',
	      		'class' => 'd-inline'
	      		]) }}
	      		<a href="{{ route('menus.show', $menu->id ) }}" class="btn btn-success">Detail</a>
	      		<a href="{{ route('menus.edit', $menu->id) }}" class="btn btn-primary">Edit</a>
	      		<button class="btn btn-danger">Delete</button>
	      	{{ Form::close() }}
	      </td>   
	    </tr>
	    @endforeach
	  </tbody>
	</table>

	
@endsection