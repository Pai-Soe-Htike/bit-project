@extends('layouts.adminlayout')
<!-- @section('title')
	Food Create
@endsection -->
@section('content')
	<h3>Foodmenu Create</h3>
	<hr>
	{{ Form::open([ 'route' => 'menus.store', 'method' => 'POST', 'enctype' => 'multipart/form-data' ]) }}
		<div class="form-group">
			{{ Form::label(null, 'Name') }}
			{{ Form::text('name', null,[
				'class' => ($errors->has('name')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('name') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Price') }}
			{{ Form::text('price', null,[
				'class' => ($errors->has('price')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('price'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('price') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Image') }}
			{{ Form::file('image_name', null,[
				'class' => ($errors->has('image_name')? 'form-control is-invalid': 'form-control'),
				'id' => 'name'
				]) }}
			@if($errors->has('image_name'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('image_name') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null,'Categories') }}
			{{ Form::select('category_id', $categories, null, [
				'class' => 
				($errors->has('category_id')? 'form-control is-invalid': 'form-control') 
				]) }}
			@if($errors->has('category_id'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('category_id') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null,'Restaurant') }}
			{{ Form::select('restaurant_id[]', $restaurants, null, [
				'class' => 
				($errors->has('restaurant_id')? 'form-control is-invalid': 'form-control'), 'multiple' => 'multiple' 
				]) }}
			@if($errors->has('restaurant_id'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('restaurant_id') }}
					</strong>
				</span>
			@endif
		</div>

		<button class="btn btn-success">Create</button>
	{{ Form::close() }}
@endsection