@extends('layouts.adminlayout')
<!-- @section('title')
	Restaurant
@endsection -->
@section('content')
	<h3>Branch Create</h3>
	<hr>
	{{ Form::open([ 'route' => 'restaurants.store', 'method' => 'POST', 'enctype' => 'multipart/form-data' ]) }}
		<div class="form-group">
			{{ Form::label(null, 'Name') }}
			{{ Form::text('name', null,[
				'class' => ($errors->has('name')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('name'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('name') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Address') }}
			{{ Form::text('address', null,[
				'class' => ($errors->has('address')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('address'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('address') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null,'Township') }}
			{{ Form::select('township_id', $townships, null, [
				'class' => 
				($errors->has('township_id')? 'form-control is-invalid': 'form-control') 
				]) }}
			@if($errors->has('township_id'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('township_id') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Email') }}
			{{ Form::text('email', null,[
				'class' => ($errors->has('email')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('email'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('email') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Image') }}
			{{ Form::file('image_name', null,[
				'class' => ($errors->has('image_name')? 'form-control is-invalid': 'form-control'),
				'id' => 'name'
				]) }}
			@if($errors->has('image_name'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('image_name') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Minimum Price') }}
			{{ Form::text('min_price', null,[
				'class' => ($errors->has('min_price')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('min_price'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('min_price') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Est Delivery Time') }}
			{{ Form::text('est_delivery_time', null,[
				'class' => ($errors->has('est_delivery_time')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('est_delivery_time'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('est_delivery_time') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Latitude') }}
			{{ Form::text('lat', null,[
				'class' => ($errors->has('lat')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('lat'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('lat') }}
					</strong>
				</span>
			@endif
		</div>
		<div class="form-group">
			{{ Form::label(null, 'Longitude') }}
			{{ Form::text('lng', null,[
				'class' => ($errors->has('lng')? 'form-control is-invalid': 'form-control')
				]) }}
			@if($errors->has('lng'))
				<span class="invalid-feedback" role="alert">
					<strong>
						{{ $errors->first('lng') }}
					</strong>
				</span>
			@endif
		</div>	
		<button class="btn btn-success mb-5">Create</button>
	{{ Form::close() }}
@endsection