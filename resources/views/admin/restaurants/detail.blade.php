@extends('layouts.adminlayout')
<!-- @section('title')
	Detail
@endsection -->
@section('content')
	@if(session()->has('message'))
		<div class="alert alert-{{ session('status') }} mt-2" role="alert">
			{{ session('message') }}
		</div>
	@endif
	<h3>{{ $restaurant->name }} Detail</h3>
	{{ Form::model($restaurant, [
		'route' => [ 'restaurants.destroy', $restaurant->id ],
		'method' => 'DELETE',
		'class' => 'd-inline'
		]) }}
		<a href="{{ route('restaurants.edit', $restaurant->id) }}" class="btn btn-primary">Edit</a>
		<button class="btn btn-danger">Delete</button>
	{{ Form::close() }}
	<hr>
	<div class="card text-center mb-3">
	  <div class="card-header">
	    {{ $restaurant->name }}
	  </div>
	  <div class="card-body">
	    <h5 class="card-title">Name = {{ $restaurant->name }}</h5>
	    <p class="card-text">Price = {{ $restaurant->address }}</p>
       	<p class="card-text">Township =
       		@if($restaurant->township)
        		{{ $restaurant->township->name }}
        	@else
        		-
        	@endif
    	</p>
	    <p class="card-text">Email = {{ $restaurant->email }}</p>
	    <p class="card-text">Email = {{ $restaurant->min_price }}</p>
	    <p class="card-text">Email = {{ $restaurant->est_delivery_time }}</p>
	    <a href="{{ $restaurant->image_url }}" target="_blank">
	       	<img src="{{ asset('uploads/'.$restaurant->image_name) }}" class="img-responsive mb-2" alt="	{{$restaurant->image_name}}" style="width: 177px; height: 117px; border-radius: 5px; border: 1px solid #000;">
	   	</a> <br>
	    <a href="javascript:history.back()" class="btn btn-primary">Back</a>
	  </div>
	  <div class="card-footer text-muted">
	    {{ Carbon\Carbon::parse($restaurant->created_at)->diffForHumans() }}
	  </div>
	</div>
	
@endsection