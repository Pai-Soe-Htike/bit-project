@extends('layouts.adminlayout')
<!-- @section('title')
	Home
@endsection -->
@section('content')
	@if(session()->has('message'))
		<div class="alert alert-{{ session('status') }} mt-2" role="alert">
			{{ session('message') }}
		</div>
	@endif
		<div style="display: flex">
			<h3 class="mr-3">Branches</h3>
			<a href="{{ route('restaurants.create') }}" class="btn btn-success">Add</a>
		</div>
	<hr>
	<table class="table">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">Name</th>
	      <th scope="col">Address</th>
	      <th scope="col">Email</th>
	      <th scope="col">Min Price</th>
	      <th scope="col">Est Delivery Time</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  	@foreach($restaurants as $restaurant)
	  	  <tr>
	  	    <td>{{ $restaurant->name }}</td>
	  	    <td>{{ $restaurant->address }}</td>
	  	    <td>{{ $restaurant->email }}</td>
	  	    <td>{{ $restaurant->min_price }}</td>
	  	    <td>{{ $restaurant->est_delivery_time }}</td>
	  	     
	  	    <td>	      	
	  	    	{{ Form::model($restaurant, [
	  	    		'route' => [ 'restaurants.destroy', $restaurant->id ],
	  	    		'method' => 'DELETE',
	  	    		'class' => 'd-inline'
	  	    		]) }}
	  	    		<a href="{{ route('restaurants.show', $restaurant->id ) }}" class="btn btn-success">Detail</a>
	  	    		<a href="{{ route('restaurants.edit', $restaurant->id) }}" class="btn btn-primary">Edit</a>
	  	    		<button class="btn btn-danger">Delete</button>
	  	    	{{ Form::close() }}
	  	    </td>   
	  	  </tr>
	  	@endforeach
	  </tbody>
	</table>

	
@endsection