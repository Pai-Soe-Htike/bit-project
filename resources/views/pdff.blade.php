<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

	<title>PDF</title>
</head>
<body>
	<table class="table">
	  <thead>
	    <tr>
	      <th scope="col">User's Name</th>
	      <th scope="col">Quantity</th>
	      <th scope="col">Ordered Date</th>
	      <th scope="col">Price</th>	      
	    </tr>
	  </thead>
	  <tbody>
	  	<?php  $total = 0; ?>
	  	@foreach($todayorder as $order)

	  		<?php $total += $order->totalprice ?>
		    <tr>
		      	<td>{{ $order->user->name }}</td>
		      	<td>{{  $order->totalquantity }}</td>		      	
		      	<td>{{  $order->orderdate }}</td>
		      	<td>{{  $order->totalprice }}</td>
		    </tr>
	    @endforeach
	  </tbody>
	  <tr>
	    	<td></td>
	    	<td></td>
	    	<th scope="col">Total</th>
	    	<th scope="col">{{ $total }}</th>
	  </tr>
	</table>
</body>
</html>