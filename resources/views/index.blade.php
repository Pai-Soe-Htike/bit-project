<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>GDNB</title>
    <!-- Scripts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="shortcut icon" href="{{ asset('/images/logo1.png') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Argon CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">    
</head>

<body>
    @if(session('email'))
      <script>
        swal("Checkout Successful!", "Check your email!", "success");
      </script>
    @endif
    <div class="back">
        <div class="d-flex bd-highlight mb-3">
            <div class="mr-auto p-2 bd-highlight">
                <a href="{{ url('/') }}">
                    <img src="{{ url('/images/logo1.png') }}" width="100px" height="auto" alt="">
                </a>
            </div>
            @guest
                <div class="p-2 bd-highlight">
                    <a class="nav-link text-uppercase" href="{{ route('login') }}">login</a>
                </div>
                <div class="p-2 bd-highlight">
                    <a class="nav-link text-uppercase" href="{{ route('register') }}">register</a>
                </div>
            @else
                <div class="p-2 bd-highlight dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-decoration: none; color: #fff; font-weight: bold;">
                      {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#">Action</a>
                        @if( Auth::user()->role_id == 1 )
                          <a class="dropdown-item" href="{{ url('admin/restaurants') }}">Dashboard</a>
                        @else
                          <a class="dropdown-item" href="{{ url('user/profile') }}">Profile</a>
                        @endif
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="{{ route('logout') }}"
                          onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                          Logout
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                    </div>
                </div>
            @endguest
        </div>
        <div class="option-box">
            <div class="holder">
                <p style="font-family: 'Nunito', sans-serif;">Locate Branches by Township</p>
                <form action="{{ url('branches') }}" method="POST" class="customSelect">
                    @csrf
                    <div class="mb-3">
                        <select class="js-example-basic-single" required name="id" id="" style="font-family: 'Nunito', sans-serif;">
                            <option value="">Select Township.</option>
                            @forelse($townships as $township)
                                <option value="{{ $township->id }}">{{ $township->name }}</option>
                            @empty
                                <option value="">Townships are not availavle</option>
                            @endforelse
                        </select>
                    </div>
                    <button style="font-family: 'Nunito', sans-serif;" class="button1" type='submit'>Locate Branches by Township!</button>
                </form>
            </div>
            <hr>
            @if ($errors->has('lat'))
                <strong>{{ $errors->first('lat') }}</strong>
            @endif
            <form action="{{ url('nearby-branches') }}" method="POST" id="demo">
                @csrf
                <button class="button2" type='submit'>Locate Nearby Branches! <i class="fas fa-search"></i></button>
            </form>
        </div>
    </div>
    <div class="steps">
        <img src="{{ url('https://res.cloudinary.com/domjrciss/image/upload/v1554140573/BIT%20Project/Untitled-1.jpg') }}" style="width: 100%;" alt="">
    </div>
    <div class="footer container-fluid" style="background: #515651;">
      <div class="row">
        <div class="col-md-9">
          <p style="color: #fff; font-weight: bold;">GDB On Social</p>
          <ul class="list-group list-group-horizontal">
            <li class="list-group-item" id="fb">Facebook</li>
            <li class="list-group-item" id="ig">Instagram</li>
            <li class="list-group-item" id="tt">Twitter</li>
            <li class="list-group-item" id="li">Linkedin</li>
          </ul>
        </div>
        <div class="col-md-3">
          <input type="text" placeholder="Email" style="border-radius: 10px; border: 1px solid #f47c48; padding: 3px 5px;">
          <button style="background: transparent; color: #fff; border-radius: 16px; cursor: pointer;">Subscribe</button>
          <p class="mt-3" style="color: #3e3e3e;">paisoehtike&copy;2019</p>
        </div>
      </div>
    </div>
    
    <!-- Load Facebook SDK for JavaScript -->
    <!-- <div id="fb-root"></div> -->
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v3.2'
        });
      };

      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your customer chat code -->
    <div class="fb-customerchat"
      attribution=setup_tool
      page_id="1593840454093081" style="position: fixed; bottom: 20px; right: 20px;">
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        window.onload = getLocation();
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
        var x = document.getElementById("demo");

        function getLocation() {
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
          } else { 
            x.innerHTML = "Geolocation is not supported by this browser.";
          }
        }

        function showPosition(position) {
          x.innerHTML += "<input type='hidden' name='lat' value='" + position.coords.latitude + "'> <input type='hidden' name='lng' value=' " + position.coords.longitude + " '> ";
        }
    </script>
</body>

</html>