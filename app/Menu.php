<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Menu extends Model implements Searchable
{
    protected $table = 'menus';
    protected $fillable = ['name', 'price', 'category_id','image_name', 'image_url'];

    public function getSearchResult(): SearchResult
    {
       $url = route('menus.show', $this->id);
       return new SearchResult(
          $this,
          $this->name,
          $url
       );
    }

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }  

    public function restaurants()
    {
    	return $this->belongsToMany('App\Restaurant');
    }  
}
