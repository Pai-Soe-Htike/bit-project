<?php 
namespace App\Services;

/**
 * 
 */
class LocationService
{
	// public function getDistance($lat1, $lng1, $lat2, $lng2)
	// {
	// 	$earthRadius = 6371000;
	// 	$latFrom = deg2rad($lat1);
	// 	$lngFrom = deg2rad($lng1);
	// 	$latTo = deg2rad($lat2);
	// 	$lngTo = deg2rad($lng2);

	// 	$lngDelta = $lngTo - $lngFrom;
	// 	$a = pow(cos($latTo) * sin($lngDelta), 2) + pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lngDelta), 2);
	// 	$b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lngDelta);
	// 	$angle = atan2(Sqrt($a), $b);
	// 	return $angle * $earthRadius;
	// }

	public function getDistance($lat1, $lng1, $lat2, $lng2)
	{
		$theta = $lng1 - $lng2;
	    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	    $dist = acos($dist);
	    $dist = rad2deg($dist);
	    $miles = $dist * 60 * 1.1515;
	    $distance=intval($miles*100)/100;

	    return $distance;
	}
}
 ?>