<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Order extends Model implements Searchable
{
    protected $table = 'orders';
    protected $fillable = ['name', 'totalprice', 'totalquantity', 'type', 'orderdate', 'ordertime', 'status', 'user_id'];

    public function getSearchResult(): SearchResult
    {
       $url = route('menus.show', $this->id);
       return new SearchResult(
          $this,
          $this->name,
          $url
       );
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
