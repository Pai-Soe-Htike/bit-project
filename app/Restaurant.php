<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Restaurant extends Model implements Searchable
{
   protected $table = 'restaurants';
   protected $fillable = [
   	'name',
   	'address',
   	'email',
      'image_name',
      'image_url',
      'min_price',
      'est_delivery_time',
      'lat',
   	'lng',
      'township_id'
   ];

   public function getSearchResult(): SearchResult
   {
      $url = route('restaurants.show', $this->id);
      return new SearchResult(
         $this,
         $this->name,
         $url
      );
   }

   public function menus()
   {
   	return $this->belongsToMany('App\Menu');
   }

   public function township()
   {
      return $this->belongsTo('App\Township');
   }
}
