<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckoutDetail extends Model
{
    protected $table = 'checkout_details';
    protected $fillable = ['name', 'quantity', 'price', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
