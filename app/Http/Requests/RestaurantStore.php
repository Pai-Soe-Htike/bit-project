<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RestaurantStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'address' => 'required',
            'email' => 'required',
            'image_name'=>'mimes:jpeg,bmp,jpg,png|between:1, 6000',
            'min_price' => 'required',
            'est_delivery_time' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ];
    }
}
