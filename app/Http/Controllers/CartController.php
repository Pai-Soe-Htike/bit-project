<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Menu;

class CartController extends Controller
{
    public function cartadd($id)
    {
    	$menu = Menu::find($id);
    	if(!$menu) {
    		abort (404);
    	}
    	$cart = session()->get('cart');
    	if(!$cart) {
    		$cart = [
    				$id => [
    						"name" => $menu->name,
    						"price" => $menu->price,
    						"quantity" => 1,
    						"image" => $menu->image_name,
    				]
    		];
    		session()->put('cart', $cart);
    		return redirect()->back()->with('success', "Menu Added Successfully!");
    	}
    	if(isset($cart[$id])) {
    		$cart[$id]['quantity']++;
    		session()->put('cart', $cart);
    		return redirect()->back()->with('success', "Menu Added Successfully!");
    	}
    	$cart[$id] = [
    		"name" => $menu->name,
    		"price" => $menu->price,
    		"quantity" => 1,
    		"image" => $menu->image_name,
    	];
    	session()->put('cart', $cart);
    	return redirect()->back()->with('success', "Menu Added Successfully!");
    }

    public function view(Request $request)
    {
        $restaurant_id = $request->id;
        $order_type = $request->type;
        return view('card', compact('restaurant_id', 'order_type'));
    }

    public function update(Request $request)
    {
        if($request->id and $request->quantity)
        {
            $cart = session()->get('cart');
 
            $cart[$request->id]["quantity"] = $request->quantity;
 
            session()->put('cart', $cart);
 
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function alldelete()
    {

        if (session('cart') != null) {
            session()->forget('cart');
        }

        return redirect()->back()->with('remove', 'All Removed successfully!');
    }

    public function remove(Request $request)
    {
       
       if($request->id) {

           $cart = session()->get('cart');

           if(isset($cart[$request->id])) {

               unset($cart[$request->id]);

               session()->put('cart', $cart);
           }

           session()->flash('success', 'Product removed successfully');
       }
    }
}
