<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Auth;
use Validator;
use App\CheckoutDetail;
use App\Order;
use App\User;
use Mail;
use PDF;

class CheckoutController extends Controller
{

    public function dailyorder() 
    {
        $today = Carbon::now()->toDateString();
        $todayorder = Order::where('orderdate', $today)->get();
        return view('admin.orders.dailyorder', compact('todayorder'));        
    }

    public function orderdetail($id) 
    {
        $user = User::find($id);
        return view('admin.orders.orderdetails', compact('user'));
    }

    public function pdf()
    {
        $today = Carbon::now()->toDateString();

        $todayorder = Order::where('orderdate', $today)->get();

        $pdf = PDF::loadView('pdff', compact('todayorder'));

        return $pdf->download('pdff.pdf'); 
    }


    public function create($type)
    {

        if(session('cart')) {
        $user = Auth()->user();
        return view('checkout', compact('user', 'type'));

        } else {

            return redirect('/');

        }
    	
    }
 
    public function store(Request $request, Faker $faker)
    {   
        $messages = [
            'name.required' => 'Need to fill this field!',
            'email.required' => 'Need to fill this field!',
            'email.email' => 'Invalid email format!',
            'phone.required' => 'Need to fill this field!',
            'address.required' => 'Need to fill this field!',
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address' => 'required',
        ], $messages)->validate();

        if($request->time == 'close') {
            return redirect()->back()->with('closed', "Restaurant was closed!");
        } else {

            $cart = session()->get('cart');
            $total = 0;
            $totalquantity = 0;

            foreach(session('cart') as $cart => $details) {

                $totalq = 0;
                $totalp = 0;

                CheckoutDetail::create([
                    'name' => $details['name'],
                    'quantity' =>  $totalq += $details['quantity'],
                    'price' =>  $totalp += $details['price'],
                    'user_id' => Auth()->user()->id,

                ]);

                $total += $details['price'] * $details['quantity'];

                $totalquantity += $details['quantity'];
                $pdfuname = Auth()->user()->name;

            }      
            
            $checkout = Order::create([
                'totalquantity' => $totalquantity,
                'totalprice' =>  $total,
                'orderdate' => $_POST['orderdate'],
                'ordertime' => $request->time,
                'type' => $_POST['type'],
                'status' => 1,
                'name' =>  $faker->numberBetween(1, 9) . date('ymd'),
                'user_id' => Auth()->user()->id,

            ]);

            
            $user = User::find(Auth()->user()->id);
            $user->name = $_POST['name'];
            $user->phone = $_POST['phone'];
            $user->address = $_POST['address'];
            $user->email = $_POST['email'];
            $user->save();

            

            $phone = $_POST['phone'];
            $email = $_POST['email'];
            $type = $_POST['type'];

            //mail
            // $today = Carbon::now()->toDateString();

            
            // $todayorder = Order::where('orderdate', $today)->get();

            $pdf = PDF::loadView('pdf', compact('pdfuname','email', 'phone', 'date', 'checkout', 'type'));

            session()->forget('cart');
            // $pdf = PDF::loadView('pdf', compact('user'));

            Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)use($pdf)
            {
                $message->subject('Thanks You for your purcheses!');
                $message->from('gdb_foodandrink@website.com', 'GDB');
                $message->to('paisoe98@gmail.com');
                $message->attachData($pdf->output(), 'pdf.pdf');
            });

            return redirect('/')->with('email', 'Checkout Successfully check you email!');
             
           
            //    Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message)
            // {
            //     $message->subject('Thanks You for your purcheses!');
            //     $message->from('gdb_foodandrink@website.com', 'GDB');
            //     $message->to('paisoe98@gmail.com');
            // });
            

            // echo "success"; 
        }
             


    }
}