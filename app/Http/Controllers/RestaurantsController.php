<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;
use App\Http\Requests\RestaurantStore;
use App\Township;
use App\Restaurant;

class RestaurantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurants = Restaurant::latest()->get();
        return view('admin.restaurants.index',compact('restaurants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $townships = Township::pluck('name', 'id')->all();
        return view('admin.restaurants.create', compact('townships'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(RestaurantStore $request)
    // {
    //     Restaurant::create($request->all());
    //     return redirect()->route('restaurants.index')
    //     ->with('message', 'Restaurant added successfully!')
    //     ->with('status', 'success');
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Restaurant $restaurant)
    {
        return view('admin.restaurants.detail', compact('restaurant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurant $restaurant)
    {
        $township = Township::pluck('name', 'id')->all();
        return view('admin.restaurants.edit',compact('restaurant', 'township'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RestaurantStore $request, Restaurant $restaurant)
    {
        $restaurant->update($request->all());
        return redirect()->route('restaurants.index')
        ->with('message', 'Successfully Updated!')
        ->with('status', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurant $restaurant)
    {
        $restaurant->delete();
        return redirect()->route('restaurants.index')
        ->with('message', 'Successfully Deleted!')
        ->with('status', 'success');
    }

    public function store(Request $request)
    {
        $image = $request->file('image_name');

        $name = $request->file('image_name')->getClientOriginalName();

        $image_name = $request->file('image_name')->getRealPath();;

        Cloudder::upload($image_name, null);

        list($width, $height) = getimagesize($image_name);

        $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);

        //save to uploads directory
        $image->move(public_path("uploads"), $name);

        //Save images
        $this->saveImages($request, $image_url);

        return redirect()->route('restaurants.index')
        ->with('message', 'Restaurant added successfully!')
        ->with('status', 'success');
    }

    public function saveImages(Request $request, $image_url)
    {
        $image = new Restaurant();
        $image->image_name = $request->file('image_name')->getClientOriginalName();
        $image->image_url = $image_url;
        $image->name = $request->name;
        $image->address = $request->address;
        $image->email = $request->email;
        $image->min_price = $request->min_price;
        $image->est_delivery_time = $request->est_delivery_time;
        $image->lat = $request->lat;
        $image->lng = $request->lng;
        $image->township_id = $request->township_id;
        $image->save();
    }
}
