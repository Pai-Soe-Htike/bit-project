<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\TownshipStore;
use App\Township;

class TownshipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $townships = Township::latest()->get();
        return view('admin.townships.index', compact('townships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.townships.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TownshipStore $request)
    {
        Township::create($request->all());
        return redirect()->route('townships.index')
        ->with('message', 'Township added successfully!')
        ->with('status', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Township $township)
    {
        return view('admin.townships.detail',compact('township'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Township $township)
    {
        return view('admin.townships.edit', compact('township'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TownshipStore $request, Township $township)
    {
        $township->update($request->all());
        return redirect()->route('townships.index')
        ->with('message', 'Successfully Updated!')
        ->with('status', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Township $township)
    {
        $township->delete();
        return redirect()->route('townships.index')
        ->with('message', 'Successfully Deleted!')
        ->with('status', 'success');
    }
}
