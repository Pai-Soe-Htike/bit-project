<?php

namespace App\Http\Controllers;

use App\Http\Requests\NearbyRequest;
use App\getLocation;
use App\Restaurant;
use App\Services\LocationService;

class GetLocationController extends Controller
{
	public function getLocation()
	{
		return view('location');
	}

    public function getNearbyBranches(LocationService $service, NearbyRequest $request)
    {
        $restaurants = Restaurant::all()->toArray();
        $curlat = $request->lat;
        $curlng = $request->lng;
        $nearbyBranches = [];

        foreach ($restaurants as $restaurant) {
            array_push($nearbyBranches, ['dis' => $service->getDistance($curlat, $curlng, $restaurant['lat'], $restaurant['lng']), 'name' => $restaurant['name'], 'id' => $restaurant['id'], 'min-price' => $restaurant['min_price'], 'image_name' => $restaurant['image_name']]);
        }

        array_multisort($nearbyBranches);
        // dd($nearbyBranches);
        return view('/nearby-branches', compact('nearbyBranches'));
    }


    // public function store(Request $request)
    // {
    // 	getLocation::Create([
    // 		'lat' => $request->lat,
    // 		'lng' => $request->lng
    // 	]);
    // 	return view('location');
    // }
}
