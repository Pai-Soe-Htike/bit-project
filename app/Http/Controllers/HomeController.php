<?php

namespace App\Http\Controllers;

use JD\Cloudder\Facades\Cloudder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Spatie\Searchable\Search;
use App\Township;
use App\User;
use App\Restaurant;
use App\Menu;
use App\Category;
use App\Order;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('location');
    }

    public function search(Request $request)
    {
        $searchResults = (new Search())
            ->registerModel(User::class, 'name')
            ->registerModel(Order::class, 'name')
            ->registerModel(Township::class, 'name')
            ->registerModel(Restaurant::class, 'name')
            ->registerModel(Menu::class, 'name')
            ->registerModel(Category::class, 'name')
            ->perform($request->input('search'));
        return view('admin/search/index', compact('searchResults'));
    }

    public function getSearchDetail(Request $request)
    {
        if ($request->type == 'Restaurant') {
            $result = Restaurant::findOrFail($request->id);
            return view('admin.restaurants.index', compact('result'));
        }
    }


    public function getUsers()
    {
        $users = User::all();
        return view('admin/users/index', compact('users'));
    }

    public function getOrders()
    {
        $orders = Order::orderBy('id', 'DESC')->get();
        return view('admin/orders/index', compact('orders'));
    }

    public function getDelivers()
    {
        $orders = Order::all();
        return view('admin/delivers/index', compact('orders'));
    }

    public function updateDeliveryStatus($id)
    {
        $order = Order::findOrFail($id);
        if ($order->status == 1) {
            Order::where('id', $id)->update(['status'=> 2]);
        } else {
            Order::where('id', $id)->update(['status'=> 1]);
        }
        return redirect()->route('admin.delivers')
        ->with('message', 'Status updated successfully!')
        ->with('status', 'success');
    }

    // public function getOrderDetail($id)
    // {
    //     $checkout_detail = CheckoutDetail::
    // }

    public function getUserDashboard()
    {
        $user = User::find(Auth::user()->id);
        return view('userprofile', compact('user'));
    }

    public function updateUserProfile(Request $request)
    {
        if ($request->old_pw == null || $request->new_pw == null) {
           session()->flash('danger', 'Password fields must not be null!');
            return redirect()->back();
        }
        if (!(Hash::check($request->old_pw, Auth::user()->password))) {
            session()->flash('danger', 'Your old password is incorrect!');
            return redirect()->back();
        }
        if (strcmp($request->old_pw, $request->new_pw) == 0) {
            session()->flash('danger', 'Your old password and new password must not be same!');
            return redirect()->back();
        }
        $user = Auth::user();
        $user->update([
            'name' => $request->name,
            'password' => Hash::make($request->new_pw),
        ]);
        session()->flash('success', 'Account updated successfully!');
        return redirect()->back();
    }

    public function welcome()
    {
        $townships = Township::all();
        return view('index', compact('townships'));
    }

    public function uploadImages(Request $request)
   {
       // $this->validate($request,[
       //     'image_name'=>'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
       // ]);

       $image_name = $request->file('image_name')->getRealPath();

       Cloudder::upload($image_name, null);

       return redirect()->back()->with('status', 'Image Uploaded Successfully');

   }

}
