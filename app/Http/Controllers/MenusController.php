<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JD\Cloudder\Facades\Cloudder;
use App\Http\Requests\MenuStore;
use App\Menu;
use App\Category;
use App\Restaurant;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::latest()->get();
        return view('admin.menus.index',compact('menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id')->all();
        $restaurants = Restaurant::pluck('name', 'id')->all();
        return view('admin.menus.create', compact('categories','restaurants'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(MenuStore $request)
    // {
    //     $menus = Menu::create($request->all());
    //     $menus->restaurants()->sync($request->restaurant_id);
    //     return redirect()->route('menus.index')
    //     ->with('message', 'Menu added successfully!')
    //     ->with('status', 'success');
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        return view('admin.menus.detail', compact('menu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $category = Category::pluck('name', 'id')->all();
        $restaurants = Restaurant::pluck('name', 'id')->all();
        $selected_restaurants = $menu->restaurants->pluck('id')->all();
        return view('admin.menus.edit',compact('menu','category', 'township', 'restaurants', 'selected_restaurants'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuStore $request, Menu $menu)
    {
        $menu->update($request->all());
        $menu->restaurants()->sync($request->restaurant_id);
        return redirect()->route('menus.index')
        ->with('message', 'Menu updated successfully!')
        ->with('status', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        return redirect()->route('menus.index')
        ->with('message', 'Successfully Deleted!')
        ->with('status', 'success');
    }

    public function store(Request $request)
    {
        $image = $request->file('image_name');

        $name = $request->file('image_name')->getClientOriginalName();

        $image_name = $request->file('image_name')->getRealPath();;

        Cloudder::upload($image_name, null);

        list($width, $height) = getimagesize($image_name);

        $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);

        //save to uploads directory
        $image->move(public_path("uploads"), $name);

        //Save images
        $this->saveImages($request, $image_url);

        return redirect()->route('menus.index')
        ->with('message', 'Menu added successfully!')
        ->with('status', 'success');
    }

    public function saveImages(Request $request, $image_url)
    {
        $image = new Menu();
        $image->image_name = $request->file('image_name')->getClientOriginalName();
        $image->image_url = $image_url;
        $image->name = $request->name;
        $image->price = $request->price;
        $image->category_id = $request->category_id;
        $image->save();
        $image->restaurants()->sync($request->restaurant_id);
    }


    public function category($r_id, $c_id, $ordertype)
    {
        $restaurant = Restaurant::findOrFail($r_id);
        $menus = $restaurant->menus;
        $categories =  Category::all();
        return view('category', compact('menus','r_id', 'c_id', 'ordertype', 'categories'));
    }
}
