<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use App\Category;
use App\Township;

class ProcessController extends Controller
{
    public function getBranches(Request $request)
    {
        $township = Township::findOrFail($request->id);
        $restaurants = $township->restaurants;
        return view('branches', compact('restaurants'));
    }

    public function getDetail(Request $request)
    {
        $restaurant = Restaurant::findOrFail($request->id);
        $ordertype = $request->type;
        $restaurant_id = $request->id;
        $menus = $restaurant->menus;
        $categories = Category::all();
        return view('detail', compact('menus', 'ordertype', 'restaurant_id', 'categories'));
    }
}
