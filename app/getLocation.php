<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class getLocation extends Model
{
    protected $table = 'get_locations';
    protected $fillable = [
        "lat",
        "lng",
    
    ];
}
