<?php 

Route::post('/upload/images', [
  'uses'   =>  'HomeController@uploadImages',
  'as'     =>  'uploadImage'
]);

 Route::get('/test', function () {
        return view('test/text');
    });


Route::get('/mainlayout', function () {
    return view('layouts/mainlayout');
});

Route::get('viewcart/{id}/{type}', 'CartController@view')->name('viewcart');

Route::get('checkout/{type}', 'CheckoutController@create')->name('checkout');

Route::get('dailyorder', 'CheckoutController@dailyorder')->name('dailyorder');
Route::get('/orderdetail/{id}', 'CheckoutController@orderdetail');

Route::get('pdf', 'CheckoutController@pdf')->name('pdf');

Route::get('store', 'CheckoutController@store')->name('store');

Route::post('store', 'CheckoutController@store');

Route::get('category/{r_id}/{c_id}/{ordertype}', 'MenusController@category');

// cartremove
    Route::get('/cart/remove/all', 'CartController@alldelete')->name('cart.alldelete');
    Route::delete('cart/remove-from-cart', 'CartController@remove')->name('cart.remove-from-cart');

 //cartupdate
    Route::patch('update-cart', 'CartController@update');

//admin auth
Route::group(['middleware' => ['admin', 'auth']], function () {
    
    
    

    Route::get('/admin', function () {
        return view('layouts/adminlayout');
    });

    

    Route::get('/home', 'HomeController@index')->name('home');

    // Route::get('/getLocation', 'GetLocationController@getLocation');

    //Search
    Route::post('admin/search', 'HomeController@search')->name('admin.search');
    Route::get('admin/search_detail/{id}/{type}', [
    'as' => 'admin.searchDetail', 'uses' => 'HomeController@getSearchDetail']);

    //menu
    Route::resource('admin/menus', 'MenusController');
    Route::resource('admin/categories', 'CategoriesController');
    Route::resource('admin/townships', 'TownshipsController');
    Route::resource('admin/restaurants', 'RestaurantsController');

    //Orders
    Route::get('admin/orders', 'HomeController@getOrders')->name('admin.orders');
    // Route::get('admin/orders/show/{id}', 'HomeController@getOrderDetail')->name('admin.orders.show');

    //Delivery
    Route::get('admin/delivers', 'HomeController@getDelivers')->name('admin.delivers');
    Route::get('admin/delivers/update/{id}', 'HomeController@updateDeliveryStatus')->name('update.delivery.status');
    //users
    Route::get('admin/users', 'HomeController@getUsers')->name('admin.users');
    Route::get('admin/profile', function () {
        return view('account');
    });
    
});


//user auth
Route::group(['middleware' => ['auth']], function () {

    // Route::get('/', 'HomeController@welcome');
    Route::get('/', 'HomeController@welcome');

    Route::get('user/profile', 'HomeController@getUserDashboard');
    Route::post('user/profile/edit', 'HomeController@updateUserProfile')->name('user.profile.edit');

    Route::post('/branches', 'ProcessController@getBranches');
    Route::get('/branches/detail/{id}/{type}', 'ProcessController@getDetail')->name('branch.detail');

    Route::post('/nearby-branches', 'GetLocationController@getNearbyBranches');
    Route::get('/nearby-branches/detail/{id}/{type}', 'ProcessController@getDetail')->name('nearby.detail');

});

Auth::routes();

//cart
    Route::get('/cartadd/{id}', 'CartController@cartadd')->name('cartadd');
