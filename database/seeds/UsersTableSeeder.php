<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Admin';
        $user->email = 'kaunghtetzaw.khz9@gmail.com';
        $user->password = Hash::make('zaw1234567');
        $user->role_id = 1;
        $user->save();

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@gmail.com';
        $user->password = Hash::make('123456');
        $user->role_id = 1;
        $user->save();

        $user = new User();
        $user->name = 'Normal User';
        $user->email = 'user@gmail.com';
        $user->password = Hash::make('123456');
        $user->role_id = 2;
        $user->save();
    }
}
