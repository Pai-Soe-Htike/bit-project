<?php

use Illuminate\Database\Seeder;
use App\Township;

class TownshipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $township = new Township();
        $township->name = 'Sanchaung';
        $township->description = 'Sutable Place for everybody';
        $township->save();

        $township = new Township();
        $township->name = 'Hlaing';
        $township->description = 'Sutable Place for everybody';
        $township->save();
    }
}
