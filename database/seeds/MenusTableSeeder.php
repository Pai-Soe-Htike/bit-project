<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu = new Menu();
        $menu->name = "Rice";
        $menu->price = "500";
        $menu->category_id = 1;
        $menu->save();

        $menu = new Menu();
        $menu->name = "CocaCola";
        $menu->price = "500";
        $menu->category_id = 2;
        $menu->save();
    }
}
