<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Restaurant;

class RestaurantsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('restaurants')->insert([
        //     'name' => 'Medicine(1)',
        //     'lat' => 16.820003,
        //     'lng' => 96.136048,
        // ]);

        // DB::table('restaurants')->insert([
        //     'name' => 'University of Foreign Languages',
        //     'lat' => 16.825148,
        //     'lng' => 96.141863,
        // ]);

        // DB::table('restaurants')->insert([
        //     'name' => 'University of Foreign Languages',
        //     'lat' => 16.833984,
        //     'lng' => 96.137571,
        // ]);

        $restaurant = new Restaurant();
        $restaurant->name = 'Pai Gyi';
        $restaurant->address = 'Gandamar Road, N0.19';
        $restaurant->email = 'kaunghtetzaw.khz9@gmail.com';
        $restaurant->min_price = 1500;
        $restaurant->est_delivery_time = '90';
        $restaurant->lat = 16.833984;
        $restaurant->lng = 96.137571;
        $restaurant->save();

        $restaurant = new Restaurant();
        $restaurant->name = 'Pro Gyi';
        $restaurant->address = 'Gandamar Road, N0.19';
        $restaurant->email = 'kaunghtetzaw.khz9@gmail.com';
        $restaurant->min_price = 1500;
        $restaurant->est_delivery_time = '90';
        $restaurant->lat = 16.833984;
        $restaurant->lng = 96.137571;
        $restaurant->save();

        $restaurant = new Restaurant();
        $restaurant->name = 'Kraken';
        $restaurant->address = 'Gandamar Road, N0.19';
        $restaurant->email = 'kaunghtetzaw.khz9@gmail.com';
        $restaurant->min_price = 1500;
        $restaurant->est_delivery_time = '90';
        $restaurant->lat = 16.833984;
        $restaurant->lng = 96.137571;
        $restaurant->save();
    }
}
