<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new Category();
        $category->name = 'Food';
        $category->description = 'Everybody eat foods';
        $category->save();

        $category = new Category();
        $category->name = 'Drink';
        $category->description = 'People need drink';
        $category->save();
    }
}
